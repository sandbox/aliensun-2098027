<?php

/**
 * @file
 * Install hooks for Eventbrite.
 */

/**
 * Implements hook_schema().
 */
function eventbrite_schema() {
  $schema['eventbrite_cache'] = array(
    'fields' => array(
      'op' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'id' => array(
        'type' => 'int',
        'size' => 'big',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'query_string' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'result' => array(
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'big',
      ),
    ),
    'primary key' => array('op', 'id', 'query_string'),
  );

  $schema['eventbrite_venue'] = array(
    'fields' => array(
      'venue_id' => array(
        'description' => "The Eventbrite venue id.",
        'type' => 'int',
        'size' => 'normal',
        'not null' => TRUE,
      ),
      'user_key' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'organizer_id' => array(
        'description' => "The Eventbrite organizer id.",
        'type' => 'int',
        'size' => 'normal',
        'not null' => FALSE,
      ),
      'status' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'address' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'address2' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'city' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'region' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'postal_code' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'country' => array(
        'type' => 'varchar',
        'length' => 2,
        'not null' => FALSE,
      ),
      'longitude' => array(
        'type' => 'float',
        'not null' => FALSE,
      ),
      'latitude' => array(
        'type' => 'float',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('venue_id'),
  );

  $schema['eventbrite_organizer'] = array(
    'fields' => array(
      'user_key' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'organizer_id' => array(
        'description' => "The Eventbrite organizer id.",
        'type' => 'int',
        'size' => 'normal',
        'not null' => TRUE,
      ),
      'status' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'url' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('organizer_id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function eventbrite_install() {
  // TODO The drupal_(un)install_schema functions are called automatically in D7.
  // drupal_install_schema('eventbrite')
}

/**
 * Remove variables on uninstall.
 */
function eventbrite_uninstall() {
  // TODO The drupal_(un)install_schema functions are called automatically in D7.
  // drupal_uninstall_schema('eventbrite')

  // No vars yet to be created by thei module
  // TODO Please review the conversion of this statement to the D7 database API syntax.
  /* db_query("DELETE FROM {variable} WHERE name LIKE 'eventbrite_%'") */
  db_delete('variable')
  ->condition('name', 'eventbrite_%', 'LIKE')
  ->execute();
  cache_clear_all('variables', 'cache');
}
